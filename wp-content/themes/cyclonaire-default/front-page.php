<?php

	/*
		Template Name: Front Page
	*/
?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <section class="site-intro">
      <div class="si-header-wrap">
        <div class="inner-wrap">
          <p class="si-h1">Hi Mike,</p>
          <p class="si-h1">Cyclonaire.net is your go-to source for company and sales information. Here you'll find the tools and resources you need for improved, knowledgeable selling. Proposal samples, training videos, application data sheets, and more - it's all here at Cyclonaire.net.</p>
        </div>
      </div>
    </section>
  </div><!-- site-header-wrap END -->


  <section class="more-info-section">
    <div class="mi-wrap">
      <div class="inner-wrap">
        <header class="section-header">
          <h2>News</h2>
        </header>
        <div class="mi-container">
          <a href="https://www.cyclonaire.com/belt-railway-of-chicago-case-study" class="mi-item">
            <h1 class="mi-header">Visit Cyclonaire at the 2016 World of Concrete Show</h1>
            <p>Cyclonaire will be exhibiting at the 2016 International Powder &amp; Bulk Solids Conference &amp; Exhibition, from May 3-5 at the Donald E. Stephens Convention Center in Rosemont, Illinois.</p>
            <div class="mi-body">
              <p>Read More</p>
            </div>
          </a>
          <a href="https://www.cyclonaire.com/international-powder-bulk-solids-conference" class="mi-item">
            <h1 class="mi-header">Do You Recognize Manufacturing Day?</h1>
            <p>This October, professionals in various industries gather to celebrate Manufacturing Day - an assortment of collaborative sessions and workshops</p>
            <div class="mi-body">
              <p>Read More</p>
            </div>
          </a>
          <a href="http://pneumatic.cyclonaire.com/identify-the-right-phase-application-ebook-download" class="mi-item" target="_blank">
            <h1 class="mi-header">That New Website Smell: <br>
            Visit Cyclonaire's Redesigned Site</h1>
            <p>Cyclonaire is excited t oannounce the launch of our fully redesigned website. encourage you to take a look for yourself - you'll find that locating information regarding our products and services is simple.</p>
            <div class="mi-body">
              <p>Read More</p>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>




  <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/offers-module' ) ); ?>    



  <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/resources-module' ) ); ?>    





<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

