<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<section class="page-header">
      <div class="ph-header-wrap">
        <div class="inner-wrap">
	       
          <h1 class="ph-h1">404: Page Not Found</h1>
        </div>
      </div>
    </section>
    </div><!-- site-header-wrap END -->    
    
    
	<section class="site-content" role="main">
	    <div class="inner-wrap">
	    	
	        <article class="site-content-primary">
				<p class="emph">Sorry but we weren't able to find the page you were looking for. </p>
				
				<p>Try looking through our <a href="<?php bloginfo('url'); ?>/sitemap">Sitemap</a> or search our website:</p>
				<form action="/" method="get" class="search-form">
          <div class="search-table">
            <div class="search-row">
              <div class="search-cell1">
                <input type="text" id="search-site" value="" placeholder="Search Website..." name="s" class="search-text" title="Search Our Site">
              </div>
              <div class="search-cell2">
                <input class="search-submit" alt="Search" title="Search" value="" type="submit">
              </div>
            </div>
          </div>
        </form>
	        </article>
	        
	       	
	    </div>
	</section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>