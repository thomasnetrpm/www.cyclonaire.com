<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<section class="page-header">
      <div class="ph-header-wrap">
        <div class="inner-wrap">
          <h1 class="ph-h1">Cyclonaire Blog</h1>
        </div>
      </div>
    </section>
    </div><!-- site-header-wrap END -->    
    
<section class="site-content" role="main">
    <div class="inner-wrap-narrow">
    	
        <article class="site-content-primary"> 
			<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				
				<article class="row">
					<figure class="col-3"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_post_thumbnail('medium'); ?></a></figure>
					<div class="col-9">
					<h3><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
					<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
					<?php the_excerpt(); ?>
					<p><?php the_tags(); ?></p>
					</div>
				</article>
				<hr>
			<?php endwhile; ?>

			<?php else: ?>
				<h2>No posts to display</h2>
				<p>No idea what happened.</p>
			<?php endif; ?>
			<?php wp_pagenavi(); ?>
		</article>
	
	</div>
</section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>



