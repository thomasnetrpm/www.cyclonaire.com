<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<section class="page-header">
      <div class="ph-header-wrap">
        <div class="inner-wrap">
	       
          <h1 class="ph-h1"><?php if(get_field('h1') ): 
      the_field('h1'); 
      else: ?> 
      	<?php the_title(); ?>
      <?php endif; ?></h1>
        </div>
      </div>
    </section>
    </div><!-- site-header-wrap END -->       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">
	    <div class="inner-wrap-narrow ">
		     <?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p class="breadcrumbs">','</p>');
} ?>
					        <article class="site-content-primary"> 
	        	
	        	<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '9' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>
				<?php endif; ?> 
				






	        </article>
	        
	     
		    

	     

			

		</div>
		 <?php if( get_field('display_child_pages')) : ?>
		     <div class="inner-wrap no-padding-top">
	<?php 
	// the query
	$the_query = new WP_Query(array( 'post_type' => 'page', 'post_parent' => $post->ID,'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title')); ?>
	
	
	<?php if ( $the_query->have_posts() ) : ?>
	<?php if( get_field('child_pages_section_header')) : ?>
		<h2><?php the_field('child_pages_section_header'); ?></h2>
	<?php endif; ?>
	<div class="rows-of-3">
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	
	<a href="<?php the_permalink(); ?>" class="loop-item">
	
	<?php if ( has_post_thumbnail() ) : ?>
	<?php the_post_thumbnail('thumbnail'); ?>
	<?php else : ?>
	<img src="<?php bloginfo('url'); ?>/assets/FPO-Cyclonaire.png" alt="FPO">
	<?php endif; ?>
	
	<h5 class="li-title"><?php the_title(); ?></h5>
	
	</a>
	   
	<?php endwhile; ?> 
	</div>
	
	<?php else : ?>
	
	
	<?php endif; ?>
	</div>
<?php endif; ?>
	</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array(  'parts/shared/offers-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>