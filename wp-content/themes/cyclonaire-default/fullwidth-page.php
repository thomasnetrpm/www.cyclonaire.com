<?php

	/*
		Template Name: Fullwidth
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<section class="page-header">
      <div class="ph-header-wrap">
        <div class="inner-wrap">
          <h1 class="ph-h1"><?php if(get_field('h1') ): 
      the_field('h1'); 
      else: ?> 
      	<?php the_title(); ?>
      <?php endif; ?></h1>
        </div>
      </div>
    </section>
    </div><!-- site-header-wrap END -->       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">
	    <div class="inner-wrap">
 
	        	<?php the_content(); ?> 
		
			 <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>

		</div>
	</section>

<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>