<?php 
  // the query
  $the_query = new WP_Query(array( 'post_type' => 'page', 'post_parent' => 966,'posts_per_page' => 5, )); ?>
  
  
  <?php if ( $the_query->have_posts() ) : ?>
 
  <div class="page-siblings">
<a href="#" class="ps-item-header">
  
  <figure class="ps-img" style="">
    <figcaption class="ps-title"><span>Other <br>Products</span></figcaption>
  </figure>
  

  
  </a>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
  
  <a href="<?php the_permalink(); ?>" class="ps-item">
  
  <figure class="ps-img" style="">
    <figcaption class="ps-title"><span><?php the_title(); ?></span></figcaption>
  </figure>
  

  
  </a>
     
  <?php endwhile; ?> 
  </div>
  
  <?php else : ?>
  
  
  <?php endif; ?>

  <?php wp_reset_postdata(); ?>