

<?php if( have_rows('offer_section','option') ): ?>
  <section class="offers-section">
    <div class="inner-wrap">
      <div class="flexslider product-carousel">
          <ul class="slides">

            <?php while( have_rows('offer_section','option') ): the_row(); 
              // vars
              $image = get_sub_field('offer_img','option');
              $header = get_sub_field('offer_header','option');
              $content = get_sub_field('offer_body','option');
              $url = get_sub_field('offer_url','option');
              $urltext = get_sub_field('offer_button_text','option');
              $size = 'thumbnail';
              ?>
              
              <li>
                <article class="os-item">
                  <div class="mo-body">
                    <h2 class="os-title"><?php echo $header; ?></h2>
                    <p class="os-description"><?php echo $content; ?></p>
                    <p class="os-cta"><a href="<?php echo $url; ?>" class="btn-important"><?php echo $urltext; ?></a></p>
                  </div>
                  <figure class="mo-img">
                    <a href="<?php echo $url; ?>">
                    	<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>">
                    </a>
                  </figure>
                </article>
              </li>                

            <?php endwhile; ?>
          </ul>
      </div>
    </div>
  </section>
<?php endif; ?>