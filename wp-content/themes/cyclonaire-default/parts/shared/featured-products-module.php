
    <section class="featured-products">
      <div class="inner-wrap">
        <header class="section-header">
        <?php if(get_field('fp_section_header','option')) : ?>
        	<h2 class="sh-header"><?php the_field('fp_section_header','option'); ?></h2>
        <?php endif; ?>
        <?php if(get_field('fp_section_subheader','option')) : ?>
        	<h3 class="sh-subheader"><?php the_field('fp_section_subheader','option'); ?></h3>
        <?php endif; ?>
      </header>
      <div class="fp-container">
        <?php

$post_object = get_field('product_1','option');

if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>

    
    <a href="<?php the_permalink(); ?>" class="fp-item" style="background-image:url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true);echo $image_url[0];  ?>)">
          
          <h3 class="fp-item-title"><?php the_title(); ?></h3>
        </a>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

        <?php

$post_object = get_field('product_2','option');

if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>

    
     <a href="<?php the_permalink(); ?>" class="fp-item" style="background-image:url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true);echo $image_url[0];  ?>)">
          <h3 class="fp-item-title"><?php the_title(); ?></h3>
        </a>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>


        <?php

$post_object = get_field('product_3','option');

if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>

    
     <a href="<?php the_permalink(); ?>" class="fp-item" style="background-image:url(<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true);echo $image_url[0];  ?>)">
          <h3 class="fp-item-title"><?php the_title(); ?></h3>
        </a>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>

      </div>
      </div>
    </section>
    


<!--<img src="<?php bloginfo('template_url'); ?>/img/fpo-feat-products-2.jpg" alt="Feat Products">-->