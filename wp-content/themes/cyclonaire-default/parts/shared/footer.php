  <!--site-wrap END -->
<!--Site Footer-->
    <footer class="site-footer" role="contentinfo">
      <div class="inner-wrap">
           <?php wp_nav_menu(array(
        'menu'            => 'Footer Nav',
        'container'       => 'nav',
        'container_class' => 'sf-nav',
        //'menu_class'      => 'sn-level-1',
        //'walker'        => new themeslug_walker_nav_menu
        )); ?>                                                     
         
         <p>© <?php echo date("Y"); ?> Cyclonaire &nbsp; Website by <a href="http://business.thomasnet.com/rpm" target="_blank">ThomasNet RPM</a></p>
      </div>
    </footer>
  </div>

