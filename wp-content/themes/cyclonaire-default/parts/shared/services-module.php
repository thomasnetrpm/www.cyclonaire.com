<section class="services-section">
<a href="<?php the_field('first_url','option'); ?>" class="ss-item">
<h2 class="ss-item-header"><span>Custom Conveying Systems</span></h2>
</a>
<a href="<?php the_field('second_url','option'); ?>" class="ss-item">
<h2 class="ss-item-header"><span>Components</span></h2>
</a>
<a href="<?php the_field('third_url','option'); ?>" class="ss-item">
<h2 class="ss-item-header"><span>Automation & Integration</span></h2>
</a>
<a href="<?php the_field('fourth_url','option'); ?>" class="ss-item">
<h2 class="ss-item-header"><span>Let’s Get Moving</span></h2>
</a>
</section>