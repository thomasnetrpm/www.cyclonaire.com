


<?php if( get_field('resources_display') == false): ?>

  <section class="resources-module">
    <div class="rm-wrap">    
      <div class="inner-wrap">
        <h2>Additional Resources</h2>
        <div class="rows-of-3">
              

          <?php if(get_field('resources_module_offer_1') ): ?>
                
                  <?php the_field('resources_module_offer_1'); ?>

              <?php elseif(get_field('global_resources_module_offer_1','option') ): ?>
              
                  <?php the_field('global_resources_module_offer_1','option'); ?>
               
               <?php endif; ?>



          <?php if(get_field('resources_module_offer_2') ): ?>
                
                  <?php the_field('resources_module_offer_2'); ?>

              <?php elseif(get_field('global_resources_module_offer_2','option') ): ?>
              
                  <?php the_field('global_resources_module_offer_2','option'); ?>
               
               <?php endif; ?>



          <?php if(get_field('resources_module_offer_3') ): ?>
                
                  <?php the_field('resources_module_offer_3'); ?>

              <?php elseif(get_field('global_resources_module_offer_3','option') ): ?>
              
                  <?php the_field('global_resources_module_offer_3','option'); ?>
               
               <?php endif; ?>

        </div> 
        <div class="center-button">
        <a href="/resource-library" class="rm-item-cta btn">View All</a>
        </div>   
      </div>
    </div>
  </section>
<?php endif; ?>
