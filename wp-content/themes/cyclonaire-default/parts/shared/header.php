<div class="site-wrap">
    <!--Site Search -->
    <div class="search-module">
      <div class="inner-wrap">
        <a href="#" target="_blank" class="search-link search-exit active"><img src="<?php bloginfo('template_url'); ?>/img/ico-exit-light.svg" alt="Exit"></a>
        <form action="/" method="get" class="search-form">
          <div class="search-table">
            <div class="search-row">
              <div class="search-cell1">
                <input type="text" id="search-site" value="" placeholder="Search Website..." name="s" class="search-text" title="Search Our Site">
              </div>
              <div class="search-cell2">
                <input class="search-submit" alt="Search" title="Search" value="" type="submit">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!--Site Header-->
    <div class="site-header-wrap clearfix" <?php if( get_field('header_bg')): ?>style="background-image:url(<?php the_field('header_bg'); ?>);"<?php endif; ?>>
      
    
    <header class="site-header" role="banner">
      <div class="site-utility-nav">
        <div class="inner-wrap">
          <div class="social-wrap">
            <a href="https://www.facebook.com/Cyclonaire" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="img/ico-facebook.png" alt="Facebook"></a>
            <a href="https://twitter.com/cyclonaire" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="img/ico-twitter.png" alt="Twitter"></a>
            <a href="https://www.linkedin.com/company/cyclonaire" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="img/ico-linkedin.png" alt="LinkedIn"></a>
            <a href="<?php bloginfo('url'); ?>/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="img/ico-blog.png" alt="Blog"></a>
          </div>
          <div class="sun-links">
            <a href="<?php bloginfo('url'); ?>/login/" class="ico-login btn-important">Login</a>
            <a href="<?php bloginfo('url'); ?>/members/" class="ico-members btn-important">Members</a>
            <a href="<?php bloginfo('url'); ?>/?a=logout" class="ico-logout btn-important">Log out</a>
            <a href="<?php bloginfo('url'); ?>/representative-locator" class="ico-locator btn-important">Rep<span class="text-mobile-hide">resentative</span> Locator</a>
            <a href="#" class="ico-search btn-important search-link">Search</a>
            <!--<a href="#" class="ico-menu btn-important menu-link">Menu</a>-->
          </div>
          
           <!--
          <span class="sh-icons">
                      <!--  <a class="sh-ico-search search-link" target="_blank" href="#"><span>Search</span></a>
          
            <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
         
          </span>-->
        </div>
        </div>
<div class="site-header-ribbon clearfix">

      <div class="inner-wrap">
        
      <!--  <a href="dest.html" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/site-logo.png" alt="Site Logo"></a> -->
      <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/cyclonaire-logo-inverted.svg" alt="Site Logo"></a>
      <span class="sh-icons">
                      <!--  <a class="sh-ico-search search-link" target="_blank" href="#"><span>Search</span></a>-->
          
            <a href="#menu" class="sh-ico-menu menu-link"><span>Menu</span></a>
         
          </span>
        <!--Site Nav--> 
        <div class="site-nav-container">
          <div class="snc-header">
            <a href="" class="close-menu menu-link">Close</a>
          </div>
           <?php wp_nav_menu(array(
        'menu'            => 'Primary Nav',
        'container'       => 'nav',
        'container_class' => 'site-nav',
        'menu_class'      => 'sn-level-1',
        'walker'        => new themeslug_walker_nav_menu
        )); ?>
        </div>
        <a href="" class="site-nav-container-screen menu-link">&nbsp;</a>
        <!--Site Nav END-->
      </div>
      <!--inner-wrap END-->
</div>
    </header>
