<section class="more-info-section">
      <div class="inner-wrap">
        <header class="section-header">
   
          <?php if(get_field('mi_section_header','option')) : ?>
        	<h2><?php the_field('mi_section_header','option'); ?></h2>
        <?php endif; ?>
        <?php if(get_field('mi_section_subheader','option')) : ?>
        	<h3><?php the_field('mi_section_subheader','option'); ?></h3>
        <?php endif; ?>
        </header>
        <div class="mi-container">

    
    <?php 
// the query
$the_query = new WP_Query(array( 'post_type' => 'post', 'posts_per_page' => 2, )); ?>

<?php if ( $the_query->have_posts() ) : ?>


<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>



 <a href="<?php echo get_permalink(); ?>" class="mi-item">
            <figure class="mi-img">
              <img src="<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'thumbnail', true);echo $image_url[0];  ?>" alt="">
            </figure>
            <div class="mi-body">
              <p><?php the_title(); ?></p>
            </div>
          </a>
          


<?php endwhile; ?> 


<?php endif; ?>


	<?php while( have_rows('additional_items','option') ): the_row(); 

		// vars
		$image = get_sub_field('mi_thumb','option');
		$title = get_sub_field('mi_title','option');
		
		$url = get_sub_field('mi_url','option');
		

		?>

<a href="<?php echo $url; ?>" class="mi-item" target="_blank">
            <figure class="mi-img">
             <?php 

 ?>

	<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt'] ?>">



            </figure>
            <div class="mi-body">
              <p><?php echo $title; ?></p>
            </div>
          </a>
		
				
		
		

	<?php endwhile; ?>
	
	
	


        </div>
      </div>
    </section>
