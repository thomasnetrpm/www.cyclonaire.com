<section class="about-section">
        <div class="inner-wrap">
          <div class="as-container">
                         <header class="section-header">
              <h2 class="sh-header">
                <?php the_field('about_section_header','option'); ?>
              </h2>
            </header>
            <p class="emph"><?php the_field('about_section_subheader','option'); ?></p>
            
                           <?php the_field('about_body','option'); ?>

            
            <p class="medium-text"><a href="<?php the_field('about_url','option'); ?>" class="arrow-link">Learn More</a></p>
            
          </div>
        </div>
        <figure class="as-figure">
          <img src="<?php bloginfo('template_url'); ?>/img/about-section.jpg" alt="About Us">
        </div>
      </section>