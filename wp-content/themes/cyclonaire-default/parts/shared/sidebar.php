<!--Secondary Content-->
<aside class="site-content-secondary col-3">
<nav class="aside-nav">
	<?php
	$args = array(
	'sort_order' => 'ASC',
	'sort_column' => 'menu_order',
	); 

 HierPageWidget::hierpages_list_pages($args); ?>
</nav>

<div class="aside-ctas">
    	<!-- Aside CTAs -->
	<?php if(get_field('aside_cta') ): ?>
	<div class="cta-aside">
	<?php the_field('aside_cta'); ?>
	</div>                 
	<?php elseif(get_field('global_aside_cta','option') ): ?>
	<div class="cta-aside">
	<?php the_field('global_aside_cta','option'); ?>
	</div>
	<?php endif; ?>        
</div>	

 
	<!-- Additional Aside Content -->
	<?php if(get_field('additional_aside_content') ): ?>
		<?php the_field('additional_aside_content'); ?>
	</div>                 
	<?php elseif(get_field('global_additional_aside_content','option') ): ?>
		<?php the_field('additional_additional_aside_content'); ?>
	<?php endif; ?>

</aside>


