<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 		Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	
	register_nav_menus(array('primary' => 'Primary Navigation'));

	//Walker Class for Menus

	class themeslug_walker_nav_menu extends Walker_Nav_Menu {

	// add classes to ul sub-menus
	function start_lvl( &$output, $depth ) {
	// depth dependent classes
	$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
	$display_depth = ( $depth + 2); // because it counts the first submenu as 0
	$classes = array(
	    'sub-menu',
	    ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
	    ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
	    'sn-level-' . $display_depth
	    );
	$class_names = implode( ' ', $classes );

	// build html
	$output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
	}

	// add main/sub classes to li's and links
	function start_el( &$output, $item, $depth, $args ) {
	global $wp_query;
	$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
	$display_depth = ( $depth + 1);
		
	// depth dependent classes
	$depth_classes = array(

	    ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
	    ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
	    ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
	    'sn-li-l' . $display_depth
	);
	$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

	// passed classes
	$classes = empty( $item->classes ) ? array() : (array) $item->classes;
	$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

	// build html
	$output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

	// link attributes
	$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	$attributes .= ' class="sn-menu-link ' . ( $depth > 0 ? 'sn-sub-menu-link' : 'sn-main-menu-link' ) . '"';

	$item_output = sprintf( '%1$s<a%2$s><span>%3$s%4$s%5$s</span></a>%6$s',
	    $args->before,
	    $attributes,
	    $args->link_before,
	    apply_filters( 'the_title', $item->title, $item->ID ),
	    $args->link_after,
	    $args->after
	);

	// build html
	$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	}

	
	// Emphasize beginning of page    
	function emph_function( $atts, $content = null ) {
	return '<p class="emph">'.do_shortcode($content).'</p>';
	}
	add_shortcode('emph', 'emph_function');

	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */
// Distributor Locator Custom Post Type
function my_custom_post_type() {

	register_post_type( 
		'locator', 
		array(
			'labels'         => 	array(
				'name'               => _x( 'Representative Locator', 'post type general name' ),
				'singular_name'      => _x( 'Rep Territory', 'post type singular name' ),
				'add_new'            => _x( 'Add New', 'Territory' ),
				'add_new_item'       => __( 'Add New Territory' ),
				'edit_item'          => __( 'Edit Territory' ),
				'new_item'           => __( 'New Territory' ),
				'all_items'          => __( 'All Territories' ),
				'view_item'          => __( 'View Territory' ),
				'search_items'       => __( 'Search Territories' ),
				'not_found'          => __( 'No territories found' ),
				'not_found_in_trash' => __( 'No territories found in the Trash' ), 
				'parent_item_colon'  => '',
				'menu_name'          => 'Rep Locator'
			),
			'description'   => 'Rep Locator',
			'public'        => true,
			'menu_position' => 5,
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt','revisions' ),
			
			'has_archive'   => true,
			'rewrite' 		=> array( 'slug' => 'rep-locator','with_front' => false ),
		)
	);
	
	
	
	flush_rewrite_rules();
}

add_action( 'init', 'my_custom_post_type' );


	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head() & wp_footer()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		//Modernizr - header
		wp_register_script( 'modernizr', get_template_directory_uri().'/js/vendor/modernizr.min.js');
		wp_enqueue_script( 'modernizr' );

		//Use google jquery library instead of WordPress default - footer
    	wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', false, '1.10.2', true);
		wp_enqueue_script('jquery');

		//Both main.js and plugins.js minified - footer
		wp_register_script( 'plugins', get_template_directory_uri().'/js/production.min.js', array( 'jquery' ), '', true);
		wp_enqueue_script( 'plugins' );


		//Style.css - header
		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
	}	



	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}

	/* ========================================================================================================================
	
	Misc
	
	======================================================================================================================== */

	// Excerpt for Pages    
	add_action( 'init', 'my_add_excerpts_to_pages' );
	function my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
	}
	//Remove Website field from comments & comment styling prompt

	function remove_comment_fields($fields) {
	    unset($fields['url']);
	    return $fields;
	}
	add_filter('comment_form_default_fields','remove_comment_fields');

	function remove_comment_styling_prompt($defaults) {
		$defaults['comment_notes_after'] = '';
		return $defaults;
	}
	add_filter('comment_form_defaults', 'remove_comment_styling_prompt');


	//Remove inline image sizing
	add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
	add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

	function remove_width_attribute( $html ) {
	   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	   return $html;
	}

	//Plugin Updates
	add_filter( 'auto_update_plugin', '__return_true' );

	//Advanced Custom Fields Options	
	if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	}

	function search_filter( $query ) {
  if ( $query->is_search && $query->is_main_query() ) {
    $query->set( 'post__not_in', 1736 ); 
  }
}
add_action( 'pre_get_posts', 'search_filter' );
/* Ajax to search Repo */

add_action( 'init', 'my_script_enqueuer' );

function my_script_enqueuer() {
   wp_enqueue_script( "repo_search_script", get_template_directory_uri() . '/js/search-repo.js', array('jquery') );
   wp_localize_script( 'repo_search_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        

   //wp_enqueue_script( 'repo_search_script' );
   wp_enqueue_script( 'repo_search_script' );

}


// filter
function my_posts_where( $where ) {
	
	$where = str_replace("meta_key = 'loc_terretory_info_%", "meta_key LIKE 'loc_terretory_info_%", $where);

	return $where;
}

add_filter('posts_where', 'my_posts_where');


add_action( 'wp_ajax_search_county_results', 'search_county_results' );
add_action( 'wp_ajax_nopriv_search_county_results', 'search_county_results' );


function search_county_results() {

	if(isset($_POST['set'])){
	$states = $_POST['state'];
	//echo $counties;
	// args
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'locator',
	'meta_query'	=> array(
	'relation'		=> 'OR',
		array(
			'key'		=> 'loc_terretory_info_%_loc_sel_state',
			'value'		=> $states,
			'compare'	=> '='
		)
	)
);


// query
$the_query = new WP_Query( $args );

?>
  <?php if( $the_query->have_posts() ): ?>
  <h3>Select County:</h3>
  <select name="county" id="county">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <?php if( have_rows('loc_terretory_info') ): 
		while ( have_rows('loc_terretory_info') ) : the_row(); ?>
    <?php 
					$loc_state = get_sub_field('loc_sel_state');
					if($states == $loc_state){
							$loc_counties = get_sub_field('loc_counties');
					$loc_counties_array = explode("\n", $loc_counties);
					$loc_counties_array_filter= array_map('trim', $loc_counties_array);
					//sort($loc_counties_array_filter, SORT_STRING);
					//sort ( $loc_counties_array_filter );
					foreach ($loc_counties_array_filter as  $key => $value) {
						if($value!=''){
						echo "<option value='";
						echo $value;
						echo "\n'>";
						echo $value;
						echo "</option>";
						}
					}
					}
			
					 ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php endwhile; ?>
  </select>
  <?php endif; 

 wp_reset_query();	 // Restore global post data stomped by the_post().
 die();	
 	}
else{
	$statesr = $_POST['stater'];
	$countyr = $_POST['countyr'];
	
	//echo $counties;
	// args
$new_args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'locator',
	'meta_query'	=> array(
	'relation'		=> 'AND',
		array(
			'key'		=> 'loc_terretory_info_%_loc_sel_state',
			'value'		=> $statesr,
			'compare'	=> '='
		)
		/*array(
			'key'		=> 'loc_terretory_info_%_loc_county',
			'value'		=> $countyr,
			'compare'	=> '='
		)*/
	)
);


// query
$the_new_query = new WP_Query( $new_args );

?>
  <?php if( $the_new_query->have_posts() ): ?>
  <ul>
    <?php while ( $the_new_query->have_posts() ) : $the_new_query->the_post(); ?>
    <?php if( have_rows('loc_terretory_info') ): 
		while ( have_rows('loc_terretory_info') ) : the_row(); ?>
    <?php 
		$loc_state = get_sub_field('loc_sel_state');					
		$loc_counties = get_sub_field('loc_counties');
					$loc_counties_array = explode("\n", $loc_counties);
					$loc_counties_array_filter= array_map('trim', $loc_counties_array);
					//foreach ($loc_counties_array_filter as  $value) { 
					//print_r($loc_counties_array_filter);
						if((in_array($countyr,$loc_counties_array_filter)) && ($loc_state == $statesr) ){
						?>
   <li class='<?php the_ID(); ?>'>
<article class='rep-locator-item'><h3 class='li-title'><?php the_title(); ?></h3><p><?php if(get_field('loc_address_1')) : ?><?php the_field('loc_address_1'); ?><br><?php endif; ?> <?php if(get_field('loc_address_2')) : ?><?php the_field('loc_address_2'); ?><br><?php endif; ?>	<?php if(get_field('loc_city')) : ?><?php the_field('loc_city'); ?><?php endif; ?><?php if(get_field('loc_state')) : ?>, <?php the_field('loc_state'); ?><?php endif; ?><?php if(get_field('loc_zip')) : ?>, <?php the_field('loc_zip'); ?><?php endif; ?><br> <?php if(get_field('loc_country')) : ?><?php the_field('loc_country'); ?><br><?php endif; ?></p><p><?php if(get_field('loc_phone')) : ?><strong>Ph:</strong> <?php the_field('loc_phone'); ?><br><?php endif; ?><?php if(get_field('loc_fax')) : ?><strong>Fax:</strong> <?php the_field('loc_fax'); ?><br><?php endif; ?></p><?php if( have_rows('loc_contact') ): while( have_rows('loc_contact') ): the_row(); ?><p><strong>Contact:</strong> <?php the_sub_field('loc_contact_name'); ?><?php if(get_sub_field('loc_contact_ph')) : ?><br><strong>Ph:</strong> <?php the_sub_field('loc_contact_ph'); ?><?php endif; ?><?php if(get_sub_field('loc_contact_email')) : ?><br><strong>Email:</strong> <a href='mailto:<?php the_sub_field('loc_contact_email'); ?>'><?php the_sub_field('loc_contact_email'); ?></a><?php endif; ?></p><?php endwhile; endif; ?></article>
		</li>
    <?php }
// } ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php endwhile; ?>
  </ul>
  <?php endif; 

 wp_reset_query();

die();
}

die();
}

/*
add_action( 'wp_ajax_search_repo_result', 'search_repo_results' );
add_action( 'wp_ajax_nopriv_search_repo_result', 'search_repo_results' );



function search_repo_results() {
		 // Restore global post data stomped by the_post().
 	
}
*/
