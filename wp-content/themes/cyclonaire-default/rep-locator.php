<?php

	/*
		Template Name: Rep Locator
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<section class="page-header">
      <div class="ph-header-wrap">
        <div class="inner-wrap">
          <h1 class="ph-h1"><?php if(get_field('h1') ): 
      the_field('h1'); 
      else: ?> 
      	<?php the_title(); ?>
      <?php endif; ?></h1>
        </div>
      </div>
    </section>
    </div><!-- site-header-wrap END -->       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">

	
<div id="map"></div>
	    <!--
		    
		    <div class="inner-wrap">
 <?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p class="breadcrumbs">','</p>');
} ?>



	        	<?php the_content(); ?> 




			 <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>





		</div>
		
		-->
	</section>

<?php endwhile; ?>


<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js"></script>




<?php 
	// the query
	$the_query = new WP_Query(array( 'post_type' => 'locator', 'posts_per_page' => -1, )); ?>
	
	
	<?php if ( $the_query->have_posts() ) : ?>
	
<script type="text/javascript">




		var map = L.map('map', {
			center: [37.09024, -95.712891],
			zoom: 4,
			scrollWheelZoom: false,
			
		});


	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	
	<?php $location = get_field('loc_map'); if( !empty($location) ): ?>   
	var marker = L.marker([<?php echo $location['lat']; ?>, <?php echo $location['lng']; ?>]).addTo(map);
<?php endif; ?>
marker.bindPopup("<article class='rep-locator-item'><h3 class='li-title'><?php the_title(); ?></h3><p><?php if(get_field('loc_address_1')) : ?><?php the_field('loc_address_1'); ?><br><?php endif; ?> <?php if(get_field('loc_address_2')) : ?><?php the_field('loc_address_2'); ?><br><?php endif; ?>	<?php if(get_field('loc_city')) : ?><?php the_field('loc_city'); ?><?php endif; ?><?php if(get_field('loc_state')) : ?>, <?php the_field('loc_state'); ?><?php endif; ?><?php if(get_field('loc_zip')) : ?>, <?php the_field('loc_zip'); ?><?php endif; ?><br> <?php if(get_field('loc_country')) : ?><?php the_field('loc_country'); ?><br><?php endif; ?></p><p><?php if(get_field('loc_phone')) : ?><strong>Ph:</strong> <?php the_field('loc_phone'); ?><br><?php endif; ?><?php if(get_field('loc_fax')) : ?><strong>Fax:</strong> <?php the_field('loc_fax'); ?><br><?php endif; ?></p><?php if( have_rows('loc_contact') ): while( have_rows('loc_contact') ): the_row(); ?><p><strong>Contact:</strong> <?php the_sub_field('loc_contact_name'); ?><?php if(get_sub_field('loc_contact_ph')) : ?><br><strong>Ph:</strong> <?php the_sub_field('loc_contact_ph'); ?><?php endif; ?><?php if(get_sub_field('loc_contact_email')) : ?><br><strong>Email:</strong> <a href='mailto:<?php the_sub_field('loc_contact_email'); ?>'><?php the_sub_field('loc_contact_email'); ?></a><?php endif; ?></p><?php endwhile; endif; ?></article>");

	<?php endwhile; ?> 

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    scrollWheelZoom: 'false',
    id: 'juliangav.b5b24271',
    accessToken: 'pk.eyJ1IjoianVsaWFuZ2F2IiwiYSI6ImYwY2VmYjAzN2ZhYzE1ZWYxNTQ3MzA4MzIwY2M5Yjk1In0.vxs7nl05ZT8h-RmZrf7nDQ'
}).addTo(map);

</script>
<div class="inner-wrap">
  <div class="repo-search rows-of-2">
<div><h3>Select State:</h3> <select name="state" id="state">
				 <option value="">Select</option>
               <option value="AL">Alabama</option>
               <option value="AK">Alaska</option>
               <option value="AZ">Arizona</option>
               <option value="AR">Arkansas</option>
               <option value="CA">California</option>
               <option value="CO">Colorado</option>
               <option value="CT">Connecticut</option>
               <option value="DE">Delaware</option>
               <option value="FL">Florida</option>
               <option value="GA">Georgia</option>
               <option value="HI">Hawaii</option>
               <option value="ID">Idaho</option>
               <option value="IL">Illinois</option>
               <option value="IN">Indiana</option>
               <option value="IA">Iowa</option>
               <option value="KS">Kansas</option>
               <option value="KY">Kentucky</option>
               <option value="LA">Louisiana</option>
               <option value="ME">Maine</option>
               <option value="MD">Maryland</option>
               <option value="MA">Massachusetts</option>
               <option value="MI">Michigan</option>
               <option value="MN">Minnesota</option>
               <option value="MS">Mississippi</option>
               <option value="MO">Missouri</option>
               <option value="MT">Montana</option>
               <option value="NE">Nebraska</option>
               <option value="NV">Nevada</option>
               <option value="NH">New Hampshire</option>
               <option value="NJ">New Jersey</option>
               <option value="NM">New Mexico</option>
               <option value="NY">New York</option>
               <option value="NC">North Carolina</option>
               <option value="ND">North Dakota</option>
               <option value="OH">Ohio</option>
               <option value="OK">Oklahoma</option>
               <option value="OR">Oregon</option>
               <option value="PA">Pennsylvania</option>
               <option value="RI">Rhode Island</option>
               <option value="SC">South Carolina</option>
               <option value="SD">South Dakota</option>
               <option value="TN">Tennessee</option>
               <option value="TX">Texas</option>
               <option value="UT">Utah</option>
               <option value="VT">Vermont</option>
               <option value="VA">Virginia</option>
               <option value="WA">Washington</option>
               <option value="WV">West Virginia</option>
               <option value="WI">Wisconsin</option>
               <option value="WY">Wyoming</option>
               </select>
            For international inquiries outside of the United States, please <a href="http://pneumatic.cyclonaire.com/contact-us">click here</a> to contact Cyclonaire today.
</div>

            <div id="county-results"></div>
          
</div>
  <div id="rep-results"></div>
</div>
	<?php else : ?>
	<?php endif; ?>



<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/slidebox' ) ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/offers-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>